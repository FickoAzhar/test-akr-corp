cara menjalankan aplikasi:

1. tools :

- browser
- text editor
- node v18.12.1

2. install dependency:

- jalankan perintah: npm install

3. memulai aplikasi:

- jalankan perintah: npm start

\*info tambahan:
proses build aplikasi agak lama karena membutuhkan resource yang cukup tinggi
