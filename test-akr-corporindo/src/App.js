import React, { useState, useEffect } from "react";
import { AppBar, Toolbar, Typography, Button, Container, Card, CardContent, CardMedia, Grid, TableContainer, Table, TableHead, TableRow, TableCell, TableBody, Box } from "@mui/material";
import { Home, Person } from "@mui/icons-material";
import { BrowserRouter as Router, Routes, Route, Link } from "react-router-dom";
import axios from "axios";

const Jumbotron = ({ title }) => (
  <div style={{ marginBottom: "20px" }}>
    <Card>
      <CardMedia component="img" height="200" image="https://source.unsplash.com/random/800x600" alt="Sample" style={{ objectFit: "cover" }} />
      <CardContent>
        <Typography variant="h4">{title}</Typography>
      </CardContent>
    </Card>
  </div>
);

const BlogCard = ({ post, user }) => (
  <Card style={{ height: "100%", display: "flex", flexDirection: "column", justifyContent: "space-between" }}>
    <CardMedia component="img" height="200" image="https://source.unsplash.com/random/800x600" alt="Sample" />
    <CardContent>
      <Typography variant="h5" noWrap>
        {post.title}
      </Typography>
      <Typography variant="subtitle2">By: {user?.name}</Typography>
      <Typography variant="body2">{post.body}</Typography>
    </CardContent>
    <Button component={Link} to={`/post/${post.id}`} variant="contained" color="primary" style={{ marginBottom: "5px" }}>
      Read More
    </Button>
  </Card>
);

const BlogPage = ({ posts, users }) => (
  <Container style={{ marginTop: "20px" }}>
    <Jumbotron title={posts[0]?.title} />
    <Grid container spacing={3}>
      {posts.slice(1).map((post, index) => {
        const user = users.find((u) => u.id === post.userId);
        return (
          <Grid key={index} item xs={12} md={4}>
            <BlogCard post={post} user={user} />
          </Grid>
        );
      })}
      {posts.length > 1 && posts.length < 4 && (
        <Grid item xs={12} md={4}>
          <Box display="flex" justifyContent="center" alignItems="center" height="100%">
            <Card style={{ height: "100%" }}>
              <CardContent>
                <Typography variant="h5">Empty Card</Typography>
                <Typography variant="subtitle2">No data available</Typography>
              </CardContent>
            </Card>
          </Box>
        </Grid>
      )}
    </Grid>
  </Container>
);

const UserPage = ({ users }) => (
  <Container style={{ marginTop: "20px" }}>
    <Typography align="center" variant="h5" gutterBottom>
      Table Users
    </Typography>
    <TableContainer>
      <Table>
        <TableHead>
          <TableRow>
            <TableCell>Name</TableCell>
            <TableCell>Email</TableCell>
            <TableCell>Phone</TableCell>
            <TableCell>Address</TableCell>
            <TableCell>Company Name</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {users.map((user) => (
            <TableRow key={user.id}>
              <TableCell>{user.name}</TableCell>
              <TableCell>{user.email}</TableCell>
              <TableCell>{user.phone}</TableCell>
              <TableCell>
                {user.address.street}, {user.address.city}
              </TableCell>
              <TableCell>{user.company.name}</TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  </Container>
);

const App = () => {
  const [posts, setPosts] = useState([]);
  const [users, setUsers] = useState([]);

  useEffect(() => {
    axios.get("https://jsonplaceholder.typicode.com/users").then((response) => {
      setUsers(response.data);
    });

    axios.get("https://jsonplaceholder.typicode.com/posts").then((response) => {
      setPosts(response.data);
    });
  }, []);

  return (
    <Router>
      <div>
        <AppBar position="static">
          <Toolbar>
            <Typography variant="h6" style={{ flexGrow: 1 }}>
              Blog App
            </Typography>
            <Button component={Link} to="/" color="inherit">
              <Home /> Home
            </Button>
            <Button component={Link} to="/user" color="inherit">
              <Person /> User
            </Button>
          </Toolbar>
        </AppBar>

        <Routes>
          <Route path="/" element={<BlogPage posts={posts} users={users} />} />
          <Route path="/user" element={<UserPage users={users} />} />
        </Routes>

        <Box mt={3} py={1} bgcolor="primary.main" color="white" textAlign="center">
          &copy; 2023 FikoA Blog. All rights reserved.
        </Box>
      </div>
    </Router>
  );
};

export default App;
